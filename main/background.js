import {app, Tray, Menu, nativeImage} from 'electron';
import serve from 'electron-serve';
import {createWindow} from './helpers';

let win, tray, iconPath;
const path = require('path');
const isProd = process.env.NODE_ENV === 'production';
const execSync = require('child_process').execSync;
const {spawn} = require('child_process');
var AdmZip = require("adm-zip");
const torProjectUrl = "https://www.torproject.org/download/tor/"
const {DownloaderHelper} = require('node-downloader-helper');
const tempFolderName = "temp"
const torFolderName = "tor"

function execCommand(command) {
    try {
        const torStatus = execSync(command);
        if (torStatus)
            return true
    } catch (e) {
        return false
    }
}

function isTorInstalled() {
    if (!execCommand('tor --version && echo $?')) {
        console.log("Cannot use 'tor --version' command. Is Tor installed?")
        return false
    }
    console.log("Tor is already installed.")
    return true
}

var os = process.platform
console.log("OS detected: " + os)

if (os === "darwin") {

    if (!isTorInstalled()) {
        console.log("Trying to install tor...")
        if (execCommand('brew install tor')) {
            console.log("Tor installed!")
        } else {
            console.log("Tor installation failed! Exiting.")
            process.exit(1);
        }

        console.log("Launching Tor...")
        try {
            const torProcess = spawn('tor');
            if (torProcess) {
                console.log("Tor launched!")
            }

        } catch (e) {
            console.log("Tor launch failed.")
            process.exit(1);
        }
    }
} else if (os === "win32") {
    const axios = require('axios')
    const cheerio = require("cheerio")
    const fs = require("fs");
    var mkdirp = require('mkdirp');
    const exec = require('child_process').execFile;

    let lastestTorVersionPath = ""

    var torProcess = function () {
        console.log("Launching Tor...")

        exec(torFolderName + '/Tor/tor.exe', function(err, data) {
            console.log(err)
            console.log(data.toString());
        });

        console.log("Tor launched!")
    }

    if (!fs.existsSync(torFolderName + "/Tor/tor.exe")) {
        console.log("tor.exe doesn't exist! Downloading...")

        mkdirp.sync(tempFolderName)
        mkdirp.sync(torFolderName)

        axios
            .get(torProjectUrl)
            .then((response) => {
                const $ = cheerio.load(response.data)
                lastestTorVersionPath = $('.downloadLink').attr('href')
                const dl = new DownloaderHelper("https://www.torproject.org" + lastestTorVersionPath, tempFolderName, {fileName: filename => `tor.zip`});
                dl.on('end', () => {
                    console.log('Download Completed')
                    var zip = new AdmZip(tempFolderName + "/tor.zip");
                    zip.extractAllTo(torFolderName, true);
                    torProcess();
                })
                dl.start();
            })
            .catch((error) => {
                console.error(error)
            });

    } else {
        console.log("tor.exe already exists.")
        torProcess();
    }

}

if (isProd) {
    iconPath = path.join(__dirname + '/images/icon.ico');
    serve({directory: 'app'});
} else {
    iconPath = path.join(__dirname + '/../renderer/public/images/icon.ico');
    app.setPath('userData', `${app.getPath('userData')} (development)`);
}

(async () => {
    await app.whenReady();

    const win = createWindow('main', {
        width: 1200,
        height: 800,
        title: "TorUp",
        show: true,
        autoHideMenuBar: true,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
        }
    });

    win.once("ready-to-show", () => win.show());
    //win.on("closed", () => (win = null));
    tray = new Tray(nativeImage.createFromPath(iconPath));
    tray.setToolTip("TorUp");
    const contextMenu = Menu.buildFromTemplate([
        {
            label: "Afficher",
            type: "normal",
            click() {
                win.show();
            }
        },
        {
            label: "Quitter",
            type: "normal",
            click() {
                win.destroy();
            }
        }
    ]);
    tray.on("click", () => (win.isVisible() ? win.hide() : win.show()));
    tray.setContextMenu(contextMenu);
    win.on("close", e => {
        if (win.isVisible()) {
            win.hide();
            e.preventDefault();
        }
    });
    if (isProd) {
        await win.loadURL('app://./home.html');
    } else {
        const port = process.argv[2];
        await win.loadURL(`http://localhost:${port}/home`);
        //win.webContents.openDevTools();
    }
})();

app.on("activate", () => {
    if (win == null) createWindow();
});

app.on('window-all-closed', () => {
    app.quit();
});
