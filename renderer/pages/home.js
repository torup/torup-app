import Head from 'next/head';
import Nav from "../components/Nav";
import {createContext} from "react";
import {torSetup} from 'tor-axios';
import isPortReachable from 'is-port-reachable';

export const TorStatus = createContext(undefined)

var net = require('net');

export function getStaticProps(context) {
    var tr = require('tor-request');
    var detectTor = require('detect-tor');
    tr.setTorAddress('127.0.0.1', 9050)
    let isUsingTor = false
    let currentAddress = null

    tr.request('https://api.ipify.org', function (err, res, body) {
        if (!err && res.statusCode == 200) {
            currentAddress = body;
            isUsingTor = detectTor.isTor(body);
        }
        this.setState({ items: body });
    });

    return {
        props: {
            ip: {
                address: currentAddress,
                isUsingTor: isUsingTor
            },
        }
    }
}

function Home({ip}) {
    return (
        <>
            <TorStatus.Provider value={{'torStatus': ip}}>

                <main className="bg-gray-800 h-screen overflow-hidden relative">
                    <div className="flex items-start justify-between">
                        <Nav/>
                    </div>
                </main>
            </TorStatus.Provider>
        </>
    );
}

export default Home;
