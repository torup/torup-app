import '../styles/globals.css'
import Head from 'next/head'

function TorUpApp({Component, pageProps}) {
    return (
        <>
            <Head>
                <title>TorUp</title>
            </Head>
            <Component {...pageProps} />
        </>
    )
}

export default TorUpApp
