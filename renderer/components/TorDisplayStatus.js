import React from 'react';
import {TorStatus} from "../pages/home";
import Footer from "./Footer";
import {useContext} from "react";
import isPortReachable from 'is-port-reachable';

var net = require('net');

function test() {
    var tr = require('tor-request');
    tr.setTorAddress('127.0.0.1', 9050)
    tr.request('https://api.ipify.org', function (err, res, body) {
        if (!err && res.statusCode == 200) {
            console.log("Your public (through Tor) IP is: " + body);
            return body.data
        }
    });
}


function TorDisplayStatus() {
    const {torStatus} = useContext(TorStatus);

    var tr = require('tor-request');
    tr.setTorAddress('127.0.0.1', 9050)

    const handleClick = (event) => {
        event.preventDefault();
        tr.request('https://api.ipify.org', function (err, res, body) {
            if (!err && res.statusCode == 200) {
                console.log(body);
            }
        });
    };

    return (
        <>
            <div className={"absolute inset-x-0 bottom-0 mx-8 pb-6 text-sm text-center divide-y divide-gray-500"}>
                <div className={"flex flex-row items-center justify-center py-2"}>
                    <span
                        className={(torStatus.isUsingTor ? 'bg-green-400' : 'bg-red-600') + " inline-block w-2 h-2 mr-2 rounded-full"}></span>
                    <p>Réseau Tor ({torStatus.address ? torStatus.address : 'Hors ligne'})</p>
                    <button onClick={handleClick}>Test</button>
                </div>
                <Footer/>
            </div>
        </>
    )
}

export default TorDisplayStatus;
