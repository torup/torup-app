export default function Footer() {
    var pjson = require('../../package.json');
    const currentYear = new Date().getFullYear()

    return (
        <>
            <p className={"pt-2"}><a
                href="https://gitlab.com/torup/torup-app">TorUp</a> © {currentYear} | {pjson.version}</p>
        </>
    )
}